#include "futex.hpp"

#include <twist/thread/native_futex.hpp>
#include <twist/fiber/sync/futex.hpp>

namespace impl {

#if defined(TWIST_FIBER)

using twist::fiber::FutexWait;
using twist::fiber::FutexWake;

#else

using twist::thread::FutexWait;
using twist::thread::FutexWake;

#endif

}  // namespace impl


#if defined(TWIST_FAULTY)

#include <twist/fault/inject_fault.hpp>

int FutexWait(unsigned int* addr, int value) {
  twist::fault::InjectFault();
  return impl::FutexWait(addr, value);
  twist::fault::InjectFault();
}

int FutexWake(unsigned int* addr, int count) {
  twist::fault::InjectFault();
  return impl::FutexWake(addr, count);
  twist::fault::InjectFault();
}

#else

int FutexWait(unsigned int* addr, int value) {
  return impl::FutexWait(addr, value);
}

int FutexWake(unsigned int* addr, int count) {
  return impl::FutexWake(addr, count);
}

#endif
