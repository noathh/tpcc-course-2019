#include <benchmark/benchmark.h>

#include "mutex.hpp"
#include "std_mutex.hpp"

#include <mutex>

//using Mutex = StdMutex; 
using Mutex = solutions::Mutex;

Mutex mutex;
volatile int count = 0;

// Uncontended

static void BM_MutexUncontended(benchmark::State& state) {
  count = 0;
  for (auto _ : state) {
    mutex.Lock();
    ++count;
    mutex.Unlock();
  }
}

BENCHMARK(BM_MutexUncontended);

// Contended

static void BM_MutexContended(benchmark::State& state) {
  if (state.thread_index == 0) {
    count = 0;
  }
  for (auto _ : state) {
    mutex.Lock();
    ++count;
    mutex.Unlock();
  }
}
BENCHMARK(BM_MutexContended)
    ->UseRealTime()
    ->Threads(2)
    ->Threads(4)
    ->Threads(8);

BENCHMARK_MAIN();
