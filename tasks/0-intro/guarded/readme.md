## Защитник

Реализуйте обертку `Guarded<T>`, которая автомагически превращает произвольный класс в *потокобезопасный* (*thread-safe*).

Пример:

```cpp
Guarded<std::vector<int>> items; // vector<int> + mutex
...
// thread safe: mutex.lock() -> push_back(42) -> mutex.unlock()
items->push_back(42);
```

При этом набор защищаемых методов `Guarded`-у заранее неизвестен, он должен уметь оборачивать произвольный класс.
