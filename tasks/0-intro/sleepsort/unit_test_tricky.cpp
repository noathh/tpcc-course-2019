#include "sleepsort.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <algorithm>

using solutions::SleepSort;

TEST_SUITE(SleepSortTricky) {
  SIMPLE_T_TEST(SlowStart) {
    const int kNumInts = 100;
    const int kSameValue = 50;

    std::vector<int> ints;

    ints.push_back(kSameValue + 1);

    for (int i = 0; i < kNumInts - 1; ++i) {
      ints.push_back(kSameValue);
    }

    SleepSort(ints);

    ASSERT_EQ(ints.size(), kNumInts);
    for (int i = 0; i + 1 < kNumInts; ++i) {
      ASSERT_EQ(ints[i], kSameValue);
    }
    ASSERT_EQ(ints.back(), kSameValue + 1);
  }
}

RUN_ALL_TESTS()

