#pragma once

#include "futex.hpp"

#include <twist/stdlike/atomic.hpp>

namespace solutions {

class ConditionVariable {
 public:
  template <class Mutex>
  void Wait(Mutex& mutex) {
    // Your code goes here
    // Use mutex.unlock() / mutex.lock() to unlock/lock mutex
  }

  void NotifyOne() {
    // Your code goes here
  }

  void NotifyAll() {
    // Your code goes here
  }
};

}  // namespace solutions

