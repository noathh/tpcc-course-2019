#include "channel.hpp"

#include <twist/support/random.hpp>
#include <twist/support/sleep.hpp>

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/threading/stdlike.hpp>

#include <atomic>
#include <chrono>
#include <string>
#include <vector>

TEST_SUITE(BufferedChannel) {
  SIMPLE_T_TEST(SendOneItem) {
    solutions::BufferedChannel<std::string> channel;

    channel.Send("hello");

    std::string message;
    ASSERT_TRUE(channel.Recv(message));
    ASSERT_EQ(message, "hello");

    channel.Close();

    ASSERT_FALSE(channel.Recv(message));
  }

  SIMPLE_T_TEST(SuccessfulRecvAfterClose) {
    solutions::BufferedChannel<std::string> channel;

    channel.Send("hello");
    channel.Send("world");
    channel.Close();

    std::string message;

    ASSERT_TRUE(channel.Recv(message));
    ASSERT_EQ(message, "hello");

    ASSERT_TRUE(channel.Recv(message));
    ASSERT_EQ(message, "world");

    ASSERT_FALSE(channel.Recv(message));
  }

  SIMPLE_T_TEST(CloseEarly) {
    solutions::BufferedChannel<std::string> channel;

    channel.Close();

    std::string message;
    ASSERT_FALSE(channel.Recv(message));
    ASSERT_FALSE(channel.Recv(message));
    ASSERT_FALSE(channel.Recv(message));
  }

  SIMPLE_T_TEST(BlockingWaitUntilSend) {
    solutions::BufferedChannel<std::string> channel;

    twist::th::thread delayed_send(
      [&channel]() {
        twist::th::this_thread::sleep_for(std::chrono::seconds(1));
        channel.Send("unblock");
      });

    std::string message;
    ASSERT_TRUE(channel.Recv(message));
    twist::th::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(message, "unblock");

    delayed_send.join();
  }

  SIMPLE_T_TEST(BlockingWaitUntilClose) {
    solutions::BufferedChannel<std::string> channel;

    twist::th::thread delayed_close(
      [&channel]() {
        twist::th::this_thread::sleep_for(std::chrono::seconds(1));
        channel.Close();
      });

    std::string message;
    ASSERT_FALSE(channel.Recv(message));

    delayed_close.join();
  }

  SIMPLE_T_TEST(DelayedSendAndClose) {
    solutions::BufferedChannel<std::string> channel;

    twist::th::thread delayed_send(
      [&channel]() {
        twist::th::this_thread::sleep_for(std::chrono::seconds(1));
        channel.Send("hello");
        channel.Close();
      });

    std::string message;

    ASSERT_TRUE(channel.Recv(message));
    ASSERT_EQ(message, "hello");

    ASSERT_FALSE(channel.Recv(message));

    delayed_send.join();
  }

  SIMPLE_T_TEST(InterruptBlockingRecvs) {
    solutions::BufferedChannel<std::string> channel;

    auto recv_routine = [&channel]() {
      std::string message;
      ASSERT_FALSE(channel.Recv(message));
    };

    std::vector<twist::th::thread> consumers;
    for (size_t i = 0; i < 10; ++i) {
      consumers.emplace_back(recv_routine);
    }

    twist::th::this_thread::sleep_for(std::chrono::seconds(1));
    channel.Close();

    for (auto& t : consumers) {
      t.join();
    }
  }

  SIMPLE_T_TEST(CloseBlockingSends) {
    solutions::BufferedChannel<std::string> channel{1};

    channel.Send("message");

    std::atomic<size_t> interrupted{0};

    auto send_routine = [&]() {
      ASSERT_THROW(channel.Send("overflow"), solutions::ChannelClosed);
      ++interrupted;
    };

    std::vector<twist::th::thread> sends;
    for (size_t i = 0; i < 10; ++i) {
      sends.emplace_back(send_routine);
    }

    twist::th::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(interrupted.load(), 0);
    channel.Close();

    for (auto& t : sends) {
      t.join();
    }
  }

  SIMPLE_T_TEST(CannotSendAfterClose) {
    solutions::BufferedChannel<std::string> channel{2};

    channel.Send("a");
    channel.Send("b");
    channel.Close();

    ASSERT_THROW(channel.Send("c"), solutions::ChannelClosed);
  }

  SIMPLE_T_TEST(MultiClose) {
    solutions::BufferedChannel<std::string> channel;

    auto producer_routine = [&channel]() {
      twist::th::this_thread::sleep_for(
        std::chrono::milliseconds(twist::RandomUInteger(200, 300)));
      channel.Close();
    };

    twist::th::thread producer_1(producer_routine);
    twist::th::thread producer_2(producer_routine);

    std::string message;
    ASSERT_FALSE(channel.Recv(message));

    producer_1.join();
    producer_2.join();

    ASSERT_THROW(channel.Send("hello"), solutions::ChannelClosed);
    ASSERT_FALSE(channel.Recv(message));
  }

  SIMPLE_T_TEST(FifoSmall) {
    solutions::BufferedChannel<std::string> channel{3};

    channel.Send("hello");
    channel.Send("world");
    channel.Send("!");
    channel.Close();

    std::string message;
    ASSERT_TRUE(channel.Recv(message));
    ASSERT_EQ(message, "hello");

    ASSERT_TRUE(channel.Recv(message));
    ASSERT_EQ(message, "world");

    ASSERT_TRUE(channel.Recv(message));
    ASSERT_EQ(message, "!");

    ASSERT_FALSE(channel.Recv(message));
  }

  SIMPLE_T_TEST(FifoTwoThreads) {
    solutions::BufferedChannel<int> channel{1};

    // Producer

    auto producer_routine = [&channel]() {
      for (int i = 0; i < 100; ++i) {
        channel.Send(i);
      }
      channel.Close();
    };

    twist::th::thread producer(producer_routine);

    // Consumer

    int item;
    for (int i = 0; i < 100; ++i) {
      ASSERT_TRUE(channel.Recv(item));
      ASSERT_EQ(item, i);
    }
    ASSERT_FALSE(channel.Recv(item));

    producer.join();
  }

  SIMPLE_T_TEST(CapacityReached) {
    solutions::BufferedChannel<int> channel{10};

    for (int i = 0; i < 10; ++i) {
      channel.Send(i);
    }

    twist::th::thread delayed_close(
      [&channel]() {
        twist::th::this_thread::sleep_for(std::chrono::seconds(1));
        channel.Close();
      });

    // Timed out
    ASSERT_THROW(channel.Send(10), solutions::ChannelClosed);

    delayed_close.join();
  }

  SIMPLE_T_TEST(UnboundedCapacity) {
    solutions::BufferedChannel<int> channel;

    for (int i = 0; i < 1024; ++i) {
      channel.Send(i);  // Don't block
    }
  }

  struct MoveOnly {
    MoveOnly() = default;

    MoveOnly(const MoveOnly& that) = delete;
    MoveOnly& operator=(const MoveOnly& that) = delete;

    MoveOnly(MoveOnly&& that) = default;
    MoveOnly& operator=(MoveOnly&& that) = default;
  };

  SIMPLE_T_TEST(MoveOnly) {
    solutions::BufferedChannel<MoveOnly> channel;
    channel.Send(MoveOnly{});
    MoveOnly item;
    ASSERT_TRUE(channel.Recv(item));
  }

  // For Address sanitizer build
  SIMPLE_T_TEST(ForgottenItem) {
    solutions::BufferedChannel<std::string> channel;
    channel.Send("l");
    channel.Send("e");
    channel.Send("a");
    channel.Send("k");
  }
}

RUN_ALL_TESTS()
